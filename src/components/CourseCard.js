// import { useState } from 'react';
import { Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

// props or properties acts as a function parameter
/*
props = courseProp : { 
    description : "Nostrud velit dolor excepteur ullamco consectetur aliquip tempor. Consectetur occaecat laborum exercitation sint reprehenderit irure nulla mollit. Do dolore sint deserunt quis ut sunt ad nulla est consectetur culpa. Est esse dolore nisi consequat nostrud id nostrud sint sint deserunt dolore."
    id : "wdc001"
    name : "PHP - Laravel"
    onOffer : true
    price : 45000
}
*/
/*
courseProp = { 
    description : "Nostrud velit dolor excepteur ullamco consectetur aliquip tempor. Consectetur occaecat laborum exercitation sint reprehenderit irure nulla mollit. Do dolore sint deserunt quis ut sunt ad nulla est consectetur culpa. Est esse dolore nisi consequat nostrud id nostrud sint sint deserunt dolore."
    id : "wdc001"
    name : "PHP - Laravel"
    onOffer : true
    price : 45000
}
*/
export default function CourseCard({ courseProp }) {
	// checks to see if the data was succesfully passed.
	// console.log(props);
	// Every component recieves information in a form of an object
	// console.log(typeof props);

	// Object Deconstruction
	// destructure the data to avoid using the dot notation
	const { name, description, price, _id } = courseProp;

	/*
        3 Hooks in React
            1. useState
            2. useEffect
            3. useContext 
    */

	// Use the state hook for this component to be able to store its state
	// States are used to keep track of information related to individual components
	/*
        Syntax:
            const [getter, setter] = useState(initialGetterValue);
    */
	// getter -> stores the value. variable
	// setter -> it sets the value to be stored in the getter

	// const [count, setCount] = useState(0);
	// Using the state hook returns an array with the first element being a value and the second element as a function that's used to change the value of the first element
	// console.log(useState(0));

	// Use state hook for getting and setting the seats for this course
	// const [seats, setSeats] = useState(30);

	// Function that keeps track of the enrollees for a course
	// By default JavaScript is synchronous it executes code from the top of the file all the way to the bottom and will wait for the completion of one expression before it proceeds to the next
	// The setter function for useStates are asynchronous allowing it to execute separately from other codes in the program
	// The "setCount" function is being executed while the "console.log" is already completed resulting in the value to be displayed in the console to be behind by one count
	// function enroll(){

	//     if (seats > 0) {
	//         setCount(count + 1);
	//         console.log('Enrollees: ' + count);
	//         setSeats(seats - 1);
	//         console.log('Seats: ' + seats);

	//     } else {
	//         alert("No more seats available");
	//     };
	// }

	return (
		<Card>
			<Card.Body>
				<Card.Title>{name}</Card.Title>
				<Card.Subtitle>Description:</Card.Subtitle>
				<Card.Text>{description}</Card.Text>
				<Card.Subtitle>Price:</Card.Subtitle>
				<Card.Text>{price}</Card.Text>
				<Button variant="primary" as={Link} to={`/courseView/${_id}`}>
					Details
				</Button>
			</Card.Body>
		</Card>
	);
}
