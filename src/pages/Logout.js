import { useContext, useEffect } from 'react';
import { Navigate } from 'react-router-dom';
import UserContext from '../userContext';

export default function Logout() {
	// consume the userCOntext object and destructure it to access the user state and unsetUser function from the context provider
	const { unsetUser, setUser } = useContext(UserContext);

	// clear the localStorage of the user's information
	unsetUser();

	useEffect(() => {
		setUser({
			id: null,
		});
	});

	return <Navigate to="/login" />;
}
