import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import UserContext from '../userContext';
import Swal from 'sweetalert2';

export default function Register() {
	const { user } = useContext(UserContext);

	//an object with methods to redirect the user
	const navigate = useNavigate();

	// State hooks to store the values of our input fields
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	// State to determine whether the submit button is enabled or not
	const [isActive, setIsActive] = useState(false);

	// Check if values are successfully binded
	console.log(email);
	console.log(password1);
	console.log(password2);

	// useEffect() - whenever there is a change in our webpage
	useEffect(() => {
		// Validation to enable submit button when all fields are populated and both passwords match
		if (
			firstName !== '' &&
			lastName !== '' &&
			email !== '' &&
			mobileNo.length === 11 &&
			password1 !== '' &&
			password2 !== '' &&
			password1 === password2
		) {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [firstName, lastName, email, mobileNo, password1, password2]);

	// Function to simulate user registration
	function registerUser(e) {
		// Prevents page redirection via form submission
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({
				email: email,
			}),
		})
			.then((res) => res.json())
			.then((data) => {
				console.log(data);

				if (data === true) {
					Swal.fire({
						title: 'Duplicate email found',
						icon: 'error',
						text: 'Please provide a different email.',
					});
				} else {
					fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
						method: 'POST',
						headers: {
							'Content-Type': 'application/json',
						},
						body: JSON.stringify({
							firstName: firstName,
							lastName: lastName,
							email: email,
							mobileNo: mobileNo,
							password: password1,
						}),
					})
						.then((res) => res.json())
						.then((data) => {
							console.log(data);

							if (data === true) {
								// Clear input fields
								setFirstName('');
								setLastName('');
								setEmail('');
								setMobileNo('');
								setPassword1('');
								setPassword2('');

								Swal.fire({
									title: 'Registration successful',
									icon: 'success',
									text: 'Welcome to Zuitt!',
								});

								// Allows us to redirect the user to the login page after registering for an account
								navigate('/courses');
							} else {
								Swal.fire({
									title: 'Something wrong',
									icon: 'error',
									text: 'Please try again.',
								});
							}
						});
				}
			});
	}

	// onChange - checks if there are any changes inside of the input fields.
	// value={email} - the value stroed in the input field will come from the value inside the getter "email"
	// setEmail(e.target.value) - sets the value of the getter ëmail to the value stored in the value={email} inside the input field
	return user.email !== null ? (
		<Navigate to="/courses" />
	) : (
		<Form onSubmit={(e) => registerUser(e)}>
			<Form.Group className="mb-3" controlId="formBasicEmail">
				<Form.Label>Email address</Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter email"
					value={email}
					onChange={(e) => setEmail(e.target.value)}
				/>
				<Form.Text className="text-muted">
					We'll never share your email with anyone else.
				</Form.Text>
			</Form.Group>

			<Form.Group className="mb-3" controlId="formBasicPassword">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Password"
					value={password1}
					onChange={(e) => setPassword1(e.target.value)}
				/>
			</Form.Group>

			<Form.Group className="mb-3" controlId="formBasicPassword2">
				<Form.Label>Verify Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Verify Password"
					value={password2}
					onChange={(e) => setPassword2(e.target.value)}
				/>
			</Form.Group>

			{/*Ternary Operator*/}
			{/*
			if (isActive === true) {
				<Button variant="primary" type="submit" id="submitBtn">Submit</Button>
			} else {
				<Button variant="danger" type="submit" id="submitBtn" disabled>Submit</Button>
			}
	  	  */}
			{isActive ? (
				<Button variant="primary" type="submit" id="submitBtn">
					Submit
				</Button>
			) : (
				<Button variant="danger" type="submit" id="submitBtn" disabled>
					Submit
				</Button>
			)}
		</Form>
	);
}
