import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import UserContext from '../userContext';
import Swal from 'sweetalert2';

export default function Login() {
	// Allows us to consume the UserContext object and it's properties for user validation
	const { user, setUser } = useContext(UserContext);

	// State hooks to store the values of our input fields
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	// State to determine whether the submit button is enabled or not
	const [isActive, setIsActive] = useState(false);

	console.log(email);
	console.log(password);

	// useEffect() - whenever there is a change in our webpage
	useEffect(() => {
		// validation to enable the register button when all fields are populated and both password match.
		if (email !== '' && password !== '') {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	});

	function loginUser(e) {
		e.preventDefault();

		// process a fetch request to the corresponding backend API
		// The header information "Content-Type" is used to specify that the information being sent to the backend will be sent in the form of JSON
		// The fetch request will communicate with our backend application providing it with a stringified JSON
		// Convert the information retrieved from the backend into a JavaScript object using the ".then(res => res.json())"
		// Capture the converted data and using the ".then(data => {})"
		/*
				Syntax:
					fetch('url',{options})
					.then(res => res.json())
					.then(data => {})
			*/
		fetch('http://localhost:4000/users/login', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({
				email: email,
				password: password,
			}),
		})
			.then((res) => res.json())
			.then((data) => {
				// it is a good practice to always print out the result of our fetch request to ensure that the correct information is received in our front end application.
				console.log(data);

				// If no user information is found, the "access" property will not be available and will return undefined
				// Using the typeof operator will return a string of the data type of the variable/expression it preceeds which is why the value being compared is in a string data type
				if (typeof data.access !== 'undefined') {
					// The JWT will be used to retrieve user information across the the whole frontend application and storing it in the localStorage will allow ease of access to the user's information
					localStorage.setItem('token', data.access);
					retrieveUserDetails(data.access);

					Swal.fire({
						title: 'Authentication Successful',
						icon: 'success',
						text: 'Welcome to Zuitt!',
					});
				} else {
					Swal.fire({
						title: 'Authentication failed',
						icon: 'error',
						text: 'Check your login deatils and try again!',
					});
				}
			});

		// Set the email of the authenticated user in the local storage
		// localStorage.setItem("email", email)

		// user = {email: localStorage.getItem('email')}
		// setUser({
		// 	email: localStorage.getItem('email')
		// })

		// clear input fields
		setEmail('');
		setPassword('');

		// alert('Successful login');
	}

	const retrieveUserDetails = (token) => {
		// The token will be sent as part of the request's header information
		// We put "Bearer" in front of the token to follow implementation standards for JWTs
		fetch('http://localhost:4000/users/details', {
			headers: {
				Authorization: `Bearer ${token}`,
			},
		})
			.then((res) => res.json())
			.then((data) => {
				console.log(data);

				// Changes the global "user" state to store the "id" and the "isAdmin" property of the user which will be used for validation across the whole application
				setUser({
					id: data._id,
					isAdmin: data.isAdmin,
				});
			});
	};

	return user.id !== null ? (
		<Navigate to="/courses" />
	) : (
		<Form onSubmit={(e) => loginUser(e)}>
			<Form.Group className="mb-3" controlId="formBasicEmail">
				<Form.Label>Email address</Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter email"
					value={email}
					onChange={(e) => setEmail(e.target.value)}
				/>
				<Form.Text className="text-muted">
					We'll never share your email with anyone else.
				</Form.Text>
			</Form.Group>

			<Form.Group className="mb-3" controlId="formBasicPassword">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Password"
					value={password}
					onChange={(e) => setPassword(e.target.value)}
				/>
			</Form.Group>

			{isActive ? (
				<Button variant="primary" type="submit" id="submitBtn">
					Submit
				</Button>
			) : (
				<Button variant="danger" type="submit" id="submitBtn" disabled>
					Submit
				</Button>
			)}
		</Form>
	);
}
